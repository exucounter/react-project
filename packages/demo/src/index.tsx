import React from "react";
import { createRoot } from "react-dom/client";
import { add } from "library";

const noop = () => undefined;

type MyType<T> = T extends [`some:${infer U}`] ? U : T;

type ReturnFuncType<T extends (...args: any) => any> = T extends (
  ...args: any
) => infer R
  ? R
  : T;

type NoopType = ReturnFuncType<typeof noop>;

type AnotherType = MyType<["some:cool"]>;
type AnotherTypeTwo = MyType<["cool2"]>;

const App = () => {
  const a = add(19, 200);
  console.log(add);
  return <>{a}</>;
};

const documentRoot = document.getElementById("root");

if (documentRoot) {
  const root = createRoot(documentRoot);
  root.render(<App />);
}
