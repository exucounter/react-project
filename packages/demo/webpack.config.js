const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const NpmDtsPlugin = require("npm-dts-webpack-plugin");

const mode = process.env.MODE || "development";
const port = process.env.PORT || 3000;

module.exports = {
  entry: "./src/index.tsx",
  mode,
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
  },
  target: "web",
  devServer: {
    static: {
      directory: path.join(__dirname, "public"),
    },
    compress: true,
    port,
    client: {
      logging: "info",
    },
    open: true,
    hot: true,
  },
  stats: "errors-only",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        exclude: /"node_modules"/,
        include: [path.join(__dirname, "src")],
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"],
  },
  optimization: {
    minimize: true,
  },
  plugins: [
    new HtmlWebpackPlugin({ template: "./src/index.html", inject: "body" }),
  ],
};
